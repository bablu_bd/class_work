<?php

class BaseClass{
    function __construct(){
    print " In baseClass constructor"."<br/>";
}

}
class SubClass extends BaseClass{

    function __construct()
    {
        parent::__construct();
        print"In subclass constructor\n";
    }
}

$obj = new SubClass();
?>